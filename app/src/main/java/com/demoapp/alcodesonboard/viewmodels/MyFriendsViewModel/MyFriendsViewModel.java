package com.demoapp.alcodesonboard.viewmodels.MyFriendsViewModel;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.adapters.MyFriendsAdapter;
import com.demoapp.alcodesonboard.database.entities.MyFriends;
import com.demoapp.alcodesonboard.repositories.MyFriendsRepository;

import java.util.List;

public class MyFriendsViewModel extends AndroidViewModel {

    private MyFriendsRepository mMyFriendsRepository;

    public MyFriendsViewModel(@NonNull Application application) {
        super(application);

        mMyFriendsRepository = MyFriendsRepository.getInstance();
    }

    public LiveData<List<MyFriendsAdapter.DataHolder>> getMyFriendsAdapterListLiveData() {
        return mMyFriendsRepository.getMyFriendsAdapterListLiveData();
    }


    public LiveData<MyFriends> getMyFriendsLiveData() {
        return mMyFriendsRepository.getMyFriendsLiveData();
    }

    public void loadMyFriendsAdapterList(Context context) {
        mMyFriendsRepository.loadMyFriendsAdapterList(context);
    }

    public void addFriends(String firstName, String lastName, String email, String avatar, Boolean reloadAdapterFlag) {
        mMyFriendsRepository.addFriends(getApplication(), firstName, lastName, email, avatar, reloadAdapterFlag);
    }

    public void editFriends(Long id, String firstName, String lastName, String email, String avatar) {
        mMyFriendsRepository.editFriends(getApplication(), id, firstName, lastName, email, avatar);
    }

    public void loadMyFriendsById(Long id){
        mMyFriendsRepository.loadMyFriendsById(getApplication(), id);
    }

    public void deleteFriends(Long id) {
        mMyFriendsRepository.deleteFriends(getApplication(), id);
    }
}

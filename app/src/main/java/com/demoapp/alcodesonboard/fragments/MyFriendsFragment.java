package com.demoapp.alcodesonboard.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.MyFriendsDetailActivity;
import com.demoapp.alcodesonboard.activities.ProfileActivity;
import com.demoapp.alcodesonboard.adapters.MyFriendsAdapter;
import com.demoapp.alcodesonboard.database.entities.MyFriends;
import com.demoapp.alcodesonboard.viewmodels.MyFriendsViewModel.MyFriendsViewModel;
import com.demoapp.alcodesonboard.viewmodels.MyFriendsViewModel.MyFriendsViewModelFactory;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MyFriendsFragment extends Fragment implements MyFriendsAdapter.Callbacks {

    public static final String TAG = com.demoapp.alcodesonboard.fragments.MyFriendsFragment.class.getSimpleName();

    @BindView(R.id.recyclerview)
    protected RecyclerView mRecyclerView;

    @BindView(R.id.textview_noData)
    protected TextView noData;

    private final int REQUEST_CODE_MY_FRIENDS_DETAIL = 300;

    private Unbinder mUnbinder;
    private MyFriendsAdapter mAdapter;
    private MyFriendsViewModel mViewModel;

    public MyFriendsFragment() {
    }

    public static com.demoapp.alcodesonboard.fragments.MyFriendsFragment newInstance() {
        return new com.demoapp.alcodesonboard.fragments.MyFriendsFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_friends, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();

        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            initViewModel();
        }else{
            // Show error in popup dialog.
            new MaterialDialog.Builder(getContext())
                    .title("Error")
                    .content("No Internet Connection.")
                    .positiveText("OK")
                    .show();
        }
    }

    @Override
    public void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }

        super.onDestroy();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_my_friends, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();

        if (itemId == R.id.menu_add) {
            startActivityForResult(new Intent(getActivity(), MyFriendsDetailActivity.class), REQUEST_CODE_MY_FRIENDS_DETAIL);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_CODE_MY_FRIENDS_DETAIL && resultCode == MyFriendsDetailActivity.RESULT_CONTENT_MODIFIED) {
            // Child report content is changed, re-load list.
            mViewModel.loadMyFriendsAdapterList(getActivity());
        }
    }

    @Override
    public void onListItemClicked(MyFriendsAdapter.DataHolder data) {
        Intent intent = new Intent(getActivity(), MyFriendsDetailActivity.class);
        intent.putExtra(MyFriendsDetailActivity.EXTRA_LONG_MY_FRIENDS_ID, data.id);

        startActivityForResult(intent, REQUEST_CODE_MY_FRIENDS_DETAIL);
    }

    @Override
    public void onDeleteButtonClicked(MyFriendsAdapter.DataHolder data) {
        mViewModel.deleteFriends(data.id);
    }

    @Override
    public void startViewProfilePage(MyFriendsAdapter.DataHolder data){
        final Intent intent = new Intent(getContext(), ProfileActivity.class);
        intent.putExtra(ProfileActivity.EXTRA_LONG_PROFILE_IMAGE, data.avatar);

        startActivity(intent);
    }

    private void initView() {
        mAdapter = new MyFriendsAdapter();
        mAdapter.setCallbacks(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void initViewModel() {
//        mViewModel = ViewModelProviders.of(this).get(MyFriendsViewModel.class);
        mViewModel = new ViewModelProvider(this, new MyFriendsViewModelFactory(getActivity().getApplication())).get(MyFriendsViewModel.class);
        // Load data into adapter.
        mViewModel.loadMyFriendsAdapterList(getActivity());
        mViewModel.getMyFriendsAdapterListLiveData().observe(getViewLifecycleOwner(), new Observer<List<MyFriendsAdapter.DataHolder>>() {

            @Override
            public void onChanged(List<MyFriendsAdapter.DataHolder> dataHolders) {

                if(dataHolders.isEmpty()){
                    noData.setVisibility(View.VISIBLE);
                }else {
                    noData.setVisibility(View.GONE);
                }

                mAdapter.setData(dataHolders);
                mAdapter.notifyDataSetChanged();
            }
        });


    }
}

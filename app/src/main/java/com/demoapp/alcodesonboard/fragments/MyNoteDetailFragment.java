package com.demoapp.alcodesonboard.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.MyNoteDetailActivity;
import com.demoapp.alcodesonboard.database.entities.MyNote;
import com.demoapp.alcodesonboard.viewmodels.MyNotesViewModel.MyNotesViewModel;
import com.demoapp.alcodesonboard.viewmodels.MyNotesViewModel.MyNotesViewModelFactory;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MyNoteDetailFragment extends Fragment {

    public static final String TAG = com.demoapp.alcodesonboard.fragments.MyNoteDetailFragment.class.getSimpleName();

    private static final String ARG_LONG_MY_NOTE_ID = "ARG_LONG_MY_NOTE_ID";

    @BindView(R.id.edittext_title)
    protected TextInputEditText mEditTextTitle;

    @BindView(R.id.edittext_content)
    protected TextInputEditText mEditTextContent;

    private Unbinder mUnbinder;
    private Long mMyNoteId = 0L;

    private MyNotesViewModel mViewModel;

    public MyNoteDetailFragment() {
    }

    public static com.demoapp.alcodesonboard.fragments.MyNoteDetailFragment newInstance(long id) {
        Bundle args = new Bundle();
        args.putLong(ARG_LONG_MY_NOTE_ID, id);

        com.demoapp.alcodesonboard.fragments.MyNoteDetailFragment fragment = new com.demoapp.alcodesonboard.fragments.MyNoteDetailFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_note_detail, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Check arguments pass from previous page.
        Bundle args = getArguments();

        if (args != null) {
            mMyNoteId = args.getLong(ARG_LONG_MY_NOTE_ID, 0);
        }

        initViewModel();
    }

    @Override
    public void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }

        super.onDestroy();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_my_note_detail, menu);
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);

        if(mMyNoteId == 0){
            menu.findItem(R.id.menu_save).setTitle("Create");
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();

        if (itemId == R.id.menu_save) {
            // TODO show confirm dialog before continue. (Done)

            if(mEditTextTitle.getText().toString().isEmpty()){
                // Show error in popup dialog.
                new MaterialDialog.Builder(getActivity())
                        .title("Error")
                        .content("Empty title.")
                        .positiveText("OK")
                        .show();

            }else if (mEditTextContent.getText().toString().isEmpty()){
                // Show error in popup dialog.
                new MaterialDialog.Builder(getActivity())
                        .title("Error")
                        .content("Empty Content.")
                        .positiveText("OK")
                        .show();

            }else{
                new MaterialDialog.Builder(getActivity())
                        .title("Confirmation")
                        .content("Save?")
                        .positiveText("YES")
                        .negativeText("NO")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                // Save record and return to list.
                                if (mMyNoteId > 0) {
                                    // Update record.

                                    mViewModel.editNote(mMyNoteId, mEditTextTitle.getText().toString(),
                                            mEditTextContent.getText().toString());

                                } else {
                                    mViewModel.addNote(mEditTextTitle.getText().toString(),
                                            mEditTextContent.getText().toString());
                                }

                                Toast.makeText(getActivity(), "Note saved.", Toast.LENGTH_SHORT).show();

                                getActivity().setResult(MyNoteDetailActivity.RESULT_CONTENT_MODIFIED);
                                getActivity().finish();
                            }
                        })
                        .show();
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initViewModel() {
        mViewModel = new ViewModelProvider(this, new MyNotesViewModelFactory(getActivity().getApplication())).get(MyNotesViewModel.class);
        mViewModel.getMyNoteLiveData().observe(getViewLifecycleOwner(), new Observer<MyNote>() {

            @Override
            public void onChanged(MyNote myNote) {
                if (myNote != null) {
                    // Record found, fill to UI.
                    mEditTextTitle.setText(myNote.getTitle());
                    mEditTextContent.setText(myNote.getContent());
                } else {
                    // Record not found.
                    Toast.makeText(getActivity(), "Note not found.", Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                }
            }
        });

        if (mMyNoteId > 0) {
            mViewModel.loadMyNoteById(mMyNoteId);
        }
    }
}

package com.demoapp.alcodesonboard.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.MyFriendsActivity;
import com.demoapp.alcodesonboard.activities.MyFriendsDetailActivity;
import com.demoapp.alcodesonboard.activities.MyNoteDetailActivity;
import com.demoapp.alcodesonboard.activities.ProfileActivity;
import com.demoapp.alcodesonboard.database.entities.MyFriends;
import com.demoapp.alcodesonboard.database.entities.MyNote;
import com.demoapp.alcodesonboard.viewmodels.MyFriendsViewModel.MyFriendsViewModel;
import com.demoapp.alcodesonboard.viewmodels.MyFriendsViewModel.MyFriendsViewModelFactory;
import com.demoapp.alcodesonboard.viewmodels.MyNotesViewModel.MyNotesViewModel;
import com.demoapp.alcodesonboard.viewmodels.MyNotesViewModel.MyNotesViewModelFactory;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

public class MyFriendsDetailFragment extends Fragment {

    public static final String TAG = com.demoapp.alcodesonboard.fragments.MyFriendsDetailFragment.class.getSimpleName();

    private static final String ARG_LONG_MY_FRIENDS_ID = "ARG_LONG_MY_FRIENDS_ID";

    @BindView(R.id.profilePicture)
    protected CircleImageView profilePicture;

    @BindView(R.id.edittext_email)
    protected TextInputEditText mEditTextEmail;

    @BindView(R.id.edittext_first_name)
    protected TextInputEditText mEditTextFirstName;

    @BindView(R.id.edittext_last_name)
    protected TextInputEditText mEditTextLastName;

    @BindView(R.id.linearlayout_friendsDetailRoot)
    protected LinearLayout root;

    private Unbinder mUnbinder;
    private Long mMyFriendsId = 0L;

    private MyFriendsViewModel mViewModel;
    private String avatar;

    private ProgressDialog pDialog;

    public MyFriendsDetailFragment() {
    }

    public static com.demoapp.alcodesonboard.fragments.MyFriendsDetailFragment newInstance(long id) {
        Bundle args = new Bundle();
        args.putLong(ARG_LONG_MY_FRIENDS_ID, id);

        com.demoapp.alcodesonboard.fragments.MyFriendsDetailFragment fragment = new com.demoapp.alcodesonboard.fragments.MyFriendsDetailFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_friends_detail, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Check arguments pass from previous page.
        Bundle args = getArguments();

        if (args != null) {
            mMyFriendsId = args.getLong(ARG_LONG_MY_FRIENDS_ID, 0);
        }

        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            initViewModel();
        }else{
            // Show error in popup dialog.
            new MaterialDialog.Builder(getContext())
                    .title("Error")
                    .content("No Internet Connection.")
                    .positiveText("OK")
                    .show();
        }
    }

    @Override
    public void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }

        super.onDestroy();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_my_friends_detail, menu);
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.edittext_email)
    protected void startMyFriendsActivity() {
        /* Create the Intent */
        final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

        /* Fill it with Data */
        emailIntent.setType("plain/text");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{mEditTextEmail.getText().toString()});
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject");
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Text");

        /* Send it off to the Activity-Chooser */
        startActivity(Intent.createChooser(emailIntent, "Send mail..."));
    }

    @OnClick(R.id.profilePicture)
    protected void startViewProfilePage(){
        final Intent intent = new Intent(getContext(), ProfileActivity.class);
        intent.putExtra(ProfileActivity.EXTRA_LONG_PROFILE_IMAGE, avatar);

        startActivity(intent);
    }

    private void initViewModel() {
        mViewModel = new ViewModelProvider(this, new MyFriendsViewModelFactory(getActivity().getApplication())).get(MyFriendsViewModel.class);
        mViewModel.getMyFriendsLiveData().observe(getViewLifecycleOwner(), new Observer<MyFriends>() {
            @Override
            public void onChanged(MyFriends myFriends) {
                pDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);

                if(!pDialog.isShowing()){
                    pDialog.setMessage("Loading from server....");
                }

                pDialog.show();

                if(myFriends != null){
                    mEditTextEmail.setText(myFriends.getEmail());
                    mEditTextFirstName.setText(myFriends.getFirstName());
                    mEditTextLastName.setText(myFriends.getLastName());
                    Glide.with(root).load(myFriends.getAvatar()).into(profilePicture);
                    avatar = myFriends.getAvatar();
                }else{
                    // Record not found.
                    Toast.makeText(getActivity(), "Friends not found.", Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                }
                pDialog.dismiss();
            }
        });

        if (mMyFriendsId > 0) {
            mViewModel.loadMyFriendsById(mMyFriendsId);
        }
    }
}

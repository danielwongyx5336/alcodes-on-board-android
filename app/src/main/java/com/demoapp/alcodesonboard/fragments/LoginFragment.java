package com.demoapp.alcodesonboard.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.demoapp.alcodesonboard.BuildConfig;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.MainActivity;
import com.demoapp.alcodesonboard.gsonmodels.LoginModel;
import com.demoapp.alcodesonboard.utils.NetworkHelper;
import com.demoapp.alcodesonboard.utils.SharedPreferenceHelper;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;

public class LoginFragment extends Fragment {

    public static final String TAG = com.demoapp.alcodesonboard.fragments.LoginFragment.class.getSimpleName();

    @BindView(R.id.edittext_email)
    protected TextInputEditText mEditTextEmail;

    @BindView(R.id.edittext_password)
    protected TextInputEditText mEditTextPasword;

    @BindView(R.id.button_login)
    protected MaterialButton mButtonLogin;

    private Unbinder mUnbinder;

    private Boolean inActivityFlag = false;

    public LoginFragment() {
    }

    public static com.demoapp.alcodesonboard.fragments.LoginFragment newInstance() {
        return new com.demoapp.alcodesonboard.fragments.LoginFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (BuildConfig.DEBUG) {
            // Auto fill in credential for testing purpose.
            mEditTextEmail.setText("eve.holt@reqres.in");
            mEditTextPasword.setText("cityslicka");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        inActivityFlag = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        inActivityFlag = false;
    }

    @Override
    public void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }

        super.onDestroy();
    }

    @OnClick(R.id.button_login)
    protected void doLogin() {
        // Disable button and wait for server response.
        mButtonLogin.setEnabled(false);

        // TODO change login button's label to "Loading" (Done)
        // TODO remember reset label to "Login" when needed. (Done)

        mButtonLogin.setText("Loading");

        final String email = mEditTextEmail.getText().toString().trim();
        final String password = mEditTextPasword.getText().toString(); // Password should not trim.

        // TODO check email and password is blank or not. (Done)
        // TODO show error message and stop process if validation failed. (Done)

        if(email.isEmpty()){
            // Show error in popup dialog.
            new MaterialDialog.Builder(getActivity())
                    .title("Error")
                    .content("Empty email.")
                    .positiveText("OK")
                    .show();
            mButtonLogin.setText("Login");
            mButtonLogin.setEnabled(true);
            return;
        }
        if(password.isEmpty()){
            // Show error in popup dialog.
            new MaterialDialog.Builder(getActivity())
                    .title("Error")
                    .content("Empty password.")
                    .positiveText("OK")
                    .show();
            mButtonLogin.setText("Login");
            mButtonLogin.setEnabled(true);
            return;
        }

        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            // notify user you are online
        // Call login API.
        String url = BuildConfig.BASE_API_URL + "login";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                mButtonLogin.setText("Login");
                Toast.makeText(getActivity(), "Login success.", Toast.LENGTH_SHORT).show();

                // TODO bad practice, should check user is still staying in this app or not after server response. (Done)
                if(inActivityFlag == true) {
                    // Convert JSON string to Java object.
                    LoginModel responseModel = new GsonBuilder().create().fromJson(response, LoginModel.class);

                    // Save user's email to shared preference.
                    SharedPreferenceHelper.getInstance(getActivity())
                            .edit()
                            .putString("email", email)
                            .putString("token", responseModel.token)
                            .apply();

                    startActivity(new Intent(getActivity(), MainActivity.class));
                    getActivity().finish();
                }else{
                    Timber.e("Bak Kut teh");
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                String message = "";
                if(inActivityFlag == true) {
                    if (volleyError instanceof NetworkError) {
                        message = "Cannot connect to Internet...Please check your connection!";
                    } else if (volleyError instanceof ServerError) {
                        message = "The server could not be found. Please try again after some time!!";
                    } else if (volleyError instanceof AuthFailureError) {
                        message = "Cannot connect to Internet...Please check your connection!";
                    } else if (volleyError instanceof ParseError) {
                        message = "Parsing error! Please try again after some time!!";
                    } else if (volleyError instanceof NoConnectionError) {
                        message = "Cannot connect to Internet...Please check your connection!";
                    } else if (volleyError instanceof TimeoutError) {
                        message = "Connection TimeOut! Please check your internet connection.";
                    }

                    mButtonLogin.setText("Login");

                    // TODO bad practice, should check user is still staying in this app or not after server response. (Done)

                    // Show error in popup dialog.
                    new MaterialDialog.Builder(getActivity())
                            .title("Error")
                            .content(message + "")
                            .positiveText("OK")
                            .show();

                    mButtonLogin.setEnabled(true);
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);

                return params;
            }
        };

        NetworkHelper.getRequestQueueInstance(getActivity()).add(stringRequest);
        } else {
            // notify user you are not online
            // Show error in popup dialog.
            new MaterialDialog.Builder(getActivity())
                    .title("Error")
                    .content("No Internet Connection.")
                    .positiveText("OK")
                    .show();
            mButtonLogin.setText("Login");
            mButtonLogin.setEnabled(true);
        }
    }
}

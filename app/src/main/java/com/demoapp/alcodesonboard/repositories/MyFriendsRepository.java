package com.demoapp.alcodesonboard.repositories;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.demoapp.alcodesonboard.BuildConfig;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.MyFriendsActivity;
import com.demoapp.alcodesonboard.adapters.MyFriendsAdapter;
import com.demoapp.alcodesonboard.database.entities.MyFriends;
import com.demoapp.alcodesonboard.database.entities.MyFriendsDao;
import com.demoapp.alcodesonboard.fragments.MyFriendsFragment;
import com.demoapp.alcodesonboard.gsonmodels.MyFriendsModel;
import com.demoapp.alcodesonboard.utils.DatabaseHelper;
import com.demoapp.alcodesonboard.utils.NetworkHelper;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

import android.os.Handler;

public class MyFriendsRepository {

    private static com.demoapp.alcodesonboard.repositories.MyFriendsRepository mInstance;

    private MutableLiveData<List<MyFriendsAdapter.DataHolder>> mMyFriendsAdapterListLiveData = new MutableLiveData<>();
    private MutableLiveData<MyFriends> mMyFriendsLiveData = new MutableLiveData<>();

    public static final String TAG = com.demoapp.alcodesonboard.repositories.MyFriendsRepository.class.getSimpleName();

    private MyFriendsModel responseModel;
    private MyFriendsModel responsePage2Model;

    private List<MyFriendsAdapter.DataHolder> dataHolders = new ArrayList<>();

    private ProgressDialog pDialog;

    public static com.demoapp.alcodesonboard.repositories.MyFriendsRepository getInstance() {
        if (mInstance == null) {
            synchronized (com.demoapp.alcodesonboard.repositories.MyFriendsRepository.class) {
                mInstance = new com.demoapp.alcodesonboard.repositories.MyFriendsRepository();
            }
        }

        return mInstance;
    }

    public MyFriendsRepository() {
    }

    public LiveData<List<MyFriendsAdapter.DataHolder>> getMyFriendsAdapterListLiveData() {
        return mMyFriendsAdapterListLiveData;
    }

    public MutableLiveData<MyFriends> getMyFriendsLiveData() {
        return mMyFriendsLiveData;
    }

    public void loadMyFriendsAdapterList(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            // notify user you are online
            pDialog = new ProgressDialog(context, R.style.AppCompatAlertDialogStyle);

            if (!pDialog.isShowing()) {
                pDialog.setMessage("Loading from server....");
            }
            pDialog.show();

            List<MyFriends> records = DatabaseHelper.getInstance(context)
                    .getMyFriendsDao()
                    .loadAll();

            if (records.size() > 0) {
                if(dataHolders.size() == 0){
                    for (MyFriends myFriends : records) {
                        MyFriendsAdapter.DataHolder dataHolder = new MyFriendsAdapter.DataHolder();
                        dataHolder.id = myFriends.getId();
                        dataHolder.firstName = myFriends.getFirstName();
                        dataHolder.avatar = myFriends.getAvatar();

                        dataHolders.add(dataHolder);
                    }
                } else if(records.size() != 12){
                    records.clear();
                    dataHolders.clear();
                    updateFromServerApi(context);
                    DatabaseHelper.getInstance(context).getMyFriendsDao().deleteAll();
                }

                if (pDialog.isShowing()) {
                    pDialog.dismiss();
                }
            } else{
                updateFromServerApi(context);
            }

            mMyFriendsAdapterListLiveData.setValue(dataHolders);

        }else{
            // Show error in popup dialog.
            new MaterialDialog.Builder(context)
                    .title("Error")
                    .content("No Internet Connection Unable to Update from Server.")
                    .positiveText("OK")
                    .show();
        }

    }

    private void updateFromServerApi(Context context){
        // Call login API.
        String url = BuildConfig.FRIENDS_PAGE1_URL;
        String url2 = BuildConfig.FRIENDS_PAGE2_URL;

        NetworkHelper.getRequestQueueInstance(context).add(new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                responseModel = new GsonBuilder().create().fromJson(response, MyFriendsModel.class);
                if (responseModel.data.length == 6) {
                    NetworkHelper.getRequestQueueInstance(context).add(new StringRequest(Request.Method.GET, url2, new Response.Listener<String>() {

                        @Override
                        public void onResponse(String responsePage2) {
                            responsePage2Model = new GsonBuilder().create().fromJson(responsePage2, MyFriendsModel.class);

                            MyFriendsModel dummyResponseModel = new MyFriendsModel();
                            dummyResponseModel.data = responseModel.data;

                            responseModel.data = new MyFriendsModel.data[responseModel.data.length + responsePage2Model.data.length];

                            for (int i = 0; i < responseModel.data.length; i++) {
                                if (i > 5) {
                                    responseModel.data[i] = responsePage2Model.data[i - responsePage2Model.data.length];
                                } else {
                                    responseModel.data[i] = dummyResponseModel.data[i];
                                }
                            }

                            // Create new record.
                            for (int x = 0; x < responseModel.data.length; x++) {
                                addFriends(context, responseModel.data[x].first_name,
                                        responseModel.data[x].last_name,
                                        responseModel.data[x].email,
                                        responseModel.data[x].avatar, false);
                            }

                            loadMyFriendsAdapterList(context);

                            if (pDialog.isShowing()) {
                                pDialog.dismiss();
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError errorPage2) {
                            Timber.e("d;;Update Page 2 From Server error: %s", errorPage2.getMessage());

                            if (pDialog.isShowing()) {
                                pDialog.dismiss();
                            }
                        }
                    }));
                }
                if (pDialog.isShowing()) {
                    pDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Timber.e("d;;Update From Server error: %s", error.getMessage());

                if (pDialog.isShowing()) {
                    pDialog.dismiss();
                }
            }
        }));
    }

    public void addFriends(Context context, String firstName, String lastName, String email, String avatar, Boolean reloadAdapterFlag) {
        // Create new record.
        MyFriends myFriends = new MyFriends();
        myFriends.setFirstName(firstName);
        myFriends.setLastName(lastName);
        myFriends.setEmail(email);
        myFriends.setAvatar(avatar);

        // Add record to database.
        DatabaseHelper.getInstance(context)
                .getMyFriendsDao()
                .insert(myFriends);

        // Done adding record, now re-load list.
        if(reloadAdapterFlag == true){
            loadMyFriendsAdapterList(context);
        }
    }

    public void loadMyFriendsById(Context context, Long id){
        MyFriends myfriends = DatabaseHelper.getInstance(context)
                .getMyFriendsDao()
                .load(id);

        mMyFriendsLiveData.setValue(myfriends);

    }

    public void editFriends(Context context, Long id, String firstName, String lastName, String email, String avatar) {
        MyFriendsDao myFriendsDao = DatabaseHelper.getInstance(context).getMyFriendsDao();
        MyFriends myFriends = myFriendsDao.load(id);

        // Check if record exists.
        if (myFriends != null) {
            // Record is found, now update.
            myFriends.setFirstName(firstName);
            myFriends.setLastName(lastName);
            myFriends.setEmail(email);
            myFriends.setAvatar(avatar);

            myFriendsDao.update(myFriends);

            // Done editing record, now re-load list.
            loadMyFriendsAdapterList(context);
        }
    }

    public void deleteFriends(Context context, Long id) {
        // Delete record from database.
        DatabaseHelper.getInstance(context)
                .getMyFriendsDao()
                .deleteByKey(id);

        // Done deleting record, now re-load list.
        loadMyFriendsAdapterList(context);
    }
}

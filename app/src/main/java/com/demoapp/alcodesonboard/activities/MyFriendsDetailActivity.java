package com.demoapp.alcodesonboard.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.fragments.MyFriendsDetailFragment;

import butterknife.ButterKnife;

public class MyFriendsDetailActivity extends AppCompatActivity {

    public static final String EXTRA_LONG_MY_FRIENDS_ID = "EXTRA_LONG_MY_FRIENDS_ID";

    public static final int RESULT_CONTENT_MODIFIED = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my_friends_detail);

        ButterKnife.bind(this);

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (fragmentManager.findFragmentByTag(MyFriendsDetailFragment.TAG) == null) {
            // Init fragment.
            Intent extra = getIntent();
            long friendsId = 0;

            if (extra != null) {
                friendsId = extra.getLongExtra(EXTRA_LONG_MY_FRIENDS_ID, 0);
            }

            fragmentManager.beginTransaction()
                    .replace(R.id.framelayout_fragment_holder, MyFriendsDetailFragment.newInstance(friendsId), MyFriendsDetailFragment.TAG)
                    .commit();
        }
    }
}

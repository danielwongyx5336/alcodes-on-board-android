package com.demoapp.alcodesonboard.activities;

import android.content.Intent;
import android.os.Bundle;

import com.bumptech.glide.Glide;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;


import com.demoapp.alcodesonboard.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {
    public static final String EXTRA_LONG_PROFILE_IMAGE = "EXTRA_LONG_PROFILE_IMAGE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        String avatar;
        Intent extra = getIntent();
        if (extra != null) {
            avatar = extra.getStringExtra(EXTRA_LONG_PROFILE_IMAGE);

            ConstraintLayout root = (ConstraintLayout) findViewById(R.id.constraintlayout_profile);
            CircleImageView profileView = (CircleImageView) findViewById(R.id.profilePicture);

            Glide.with(root).load(avatar).into(profileView);
        }

    }

}

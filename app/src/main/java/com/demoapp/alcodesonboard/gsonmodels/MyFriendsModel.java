package com.demoapp.alcodesonboard.gsonmodels;

public class MyFriendsModel {
    public int page;
    public int per_page;
    public int Total;
    public int total_pages;
    public data[] data;

    public static class data {
        public Long ID;
        public String email;
        public String first_name;
        public String last_name;
        public String avatar;
    }
}
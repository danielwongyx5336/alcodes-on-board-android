package com.demoapp.alcodesonboard.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.demoapp.alcodesonboard.R;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyNotesAdapter extends RecyclerView.Adapter<MyNotesAdapter.ViewHolder> {

    private List<DataHolder> mData = new ArrayList<>();
    private Callbacks mCallbacks;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_notes, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(mData.get(position), mCallbacks);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<DataHolder> data) {
        if (data == null) {
            mData = new ArrayList<>();
        } else {
            mData = data;
        }
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    public static class DataHolder {

        public Long id;
        public String title;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.linearlayout_root)
        public LinearLayout root;

        @BindView(R.id.textview_note_title)
        public TextView title;

        @BindView(R.id.button_delete)
        public MaterialButton deleteButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bindTo(DataHolder data, Callbacks callbacks) {
            resetViews();

            if (data != null) {
                title.setText(data.title);

                if (callbacks != null) {
                    if (title.getText().toString().equals("No data")) {
                        root.setOnClickListener(null);
                        deleteButton.setOnClickListener(null);
                        deleteButton.setVisibility(View.GONE);
                    } else {
                        root.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                callbacks.onListItemClicked(data);
                            }
                        });

                        deleteButton.setVisibility(View.VISIBLE);
                        deleteButton.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                callbacks.onDeleteButtonClicked(data);
                            }
                        });
                    }
                }
            }
        }

        public void resetViews() {
            title.setText("No data");
            root.setOnClickListener(null);
            deleteButton.setOnClickListener(null);
        }
    }

    public interface Callbacks {

        void onListItemClicked(DataHolder data);

        void onDeleteButtonClicked(DataHolder data);
    }
}

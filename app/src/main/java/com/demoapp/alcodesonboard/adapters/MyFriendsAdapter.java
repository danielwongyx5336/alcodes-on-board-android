package com.demoapp.alcodesonboard.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.demoapp.alcodesonboard.R;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class MyFriendsAdapter extends RecyclerView.Adapter<MyFriendsAdapter.ViewHolder> {

    private List<DataHolder> mData = new ArrayList<>();
    private Callbacks mCallbacks;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_friends, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(mData.get(position), mCallbacks);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<DataHolder> data) {
        if (data == null) {
            mData = new ArrayList<>();
        } else {
            mData = data;
        }
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    public static class DataHolder {

        public Long id;
        public String firstName;
        public String avatar;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.linearlayout_root)
        public LinearLayout root;

        @BindView(R.id.textview_friends_title)
        public TextView firstName;

        @BindView(R.id.profilePicture)
        public CircleImageView profilePicture;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bindTo(DataHolder data, Callbacks callbacks) {
            resetViews();

            if (data != null) {
                firstName.setText(data.firstName);

                if (callbacks != null) {
                    if (firstName.getText().toString().equals("No data")) {
                        root.setOnClickListener(null);
                        profilePicture.setOnClickListener(null);
                        profilePicture.setVisibility(View.GONE);
                    } else {
                        root.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                callbacks.onListItemClicked(data);
                            }
                        });

                        profilePicture.setVisibility(View.VISIBLE);
                        profilePicture.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                callbacks.startViewProfilePage(data);
                            }
                        });
                        Glide.with(root).load(data.avatar).into(profilePicture);
                    }
                }
            }
        }

        public void resetViews() {
            firstName.setText("No data");
            root.setOnClickListener(null);
            profilePicture.setOnClickListener(null);
        }
    }

    public interface Callbacks {

        void onListItemClicked(DataHolder data);

        void onDeleteButtonClicked(DataHolder data);

        void startViewProfilePage(DataHolder data);

    }
}
